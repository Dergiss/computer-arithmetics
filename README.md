# Binary system and computer arithmetics #

# Projects #

1. `BitOps`

    Displays binary word which can be manipulated by selecting single bit and performing set, clear or flip operation either on selected bit, on other bits or on the entire word.

2. `MaskOps`

    Displays two binary words, an input word and a mask, which can be edited independently and then combined to produce output word according to masked binary operations.

3. `Integers`

    Displays binary register which can be manipulated by setting its bits independently or by performing one of four arithmetic operations: incrementation, decrementation, addition and subtraction of arbitrary value. Current value of the register is displayed decimally according to unsigned and signed interpretation.

