#ifndef _DYNARRAY_H_
#define _DYNARRAY_H_

#include <stddef.h>
#include <stdbool.h>

typedef struct {
    void** elements;
    size_t size;
    size_t capacity;
} DYNARRAY;


bool dynarrNew(DYNARRAY* array);
bool dynarrAdd(DYNARRAY* array, void* pointer);
void dynarrDelete(DYNARRAY* array);

typedef enum {
    stopEnumeration = false,
    continueEnumeration = true
} dynarrEnumIterationResult;
typedef dynarrEnumIterationResult (*dynarrEnumFun)(void* element, size_t index, int* retVal, void* customParam);
int dynarrEnumerate(const DYNARRAY* array, dynarrEnumFun callback, void* customParam);

#endif