#include "interface.h"

bitword bits = 0x0000;      // displayed 16-bit binary word
int selectedBit = -1;       // index of selected bit (0-15)

void onSetSelected() {
    // TODO: Implement this function
    // set one selectedBit in bits to 1, leaving the rest unchanged
}

void onClearSelected() {
    // TODO: Implement this function
    // reset one selectedBit in bits to 0, leaving the rest unchanged
}

void onFlipSelected() {
    // TODO: Implement this function
    // negate one selectedBit in bits, leaving the rest unchanged
}

void onSetOther() {
    // TODO: Implement this function
    // set all bits to 1 except the selectedBit
}

void onClearOther() {
    // TODO: Implement this function
    // reset all bits to 0 except the selectedBit
}

void onFlipOther() {
    // TODO: Implement this function
    // negate all bits except the selectedBit
}

void onSetAll() {
    // TODO: Implement this function
    // set all bits to 1
}

void onClearAll() {
    // TODO: Implement this function
    // reset all bits to 0
}

void onFlipAll() {
    // TODO: Implement this function
    // negate all bits
}
