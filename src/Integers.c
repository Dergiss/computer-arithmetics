#include "interface.h"

enum { BITS_COUNT = 16 };

typedef unsigned char bit;

// Arrays of bit elements (each array element must contain either 0 or 1)
bit valueBits[BITS_COUNT];
bit deltaBits[BITS_COUNT];

void onIncrement() {
    // TODO: Implement this function
    // after returning from this routine valueBits array should contain
    // incremented bit pattern
}

void onDecrement() {
    // TODO: Implement this function
    // after returning from this routine valueBits array should contain
    // decremented bit pattern
}

unsigned int getUnsignedValue() {
    // TODO: Implement this function
    // return unsigned interpretation of valueBits array
}

signed int getSignedValue() {
    // TODO: Implement this function
    // return signed interpretation of valueBits array
}

void onAddDelta() {
    // TODO: Implement this function
    // after returning from this routine valueBits array should contain
    // binary sum valueBits+deltaBits
}

void onSubDelta() {
    // TODO: Implement this function
    // after returning from this routine valueBits array should contain
    // binary difference valueBits-deltaBits
}
