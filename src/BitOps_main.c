#include <stdlib.h>
#include "interface.h"

extern bitword bits;
extern int selectedBit;

bitword selection;

void onByteClick(int bit) {
    selectedBit = bit;
    selection = 1 << bit;
}

void onSetSelected();
void onClearSelected();
void onFlipSelected();
void onSetOther();
void onClearOther();
void onFlipOther();
void onSetAll();
void onClearAll();
void onFlipAll();

int main() {
    if (!initInterface()) {
        return EXIT_FAILURE;
    }

    BINARYWINDOW win;
    newBinaryWin(&win, 4, 8, "Bits", COLOR_YELLOW, 16, &bits, &selection, onByteClick);

    COMMANDWINDOW cmd[9];
    COMMANDWINDOW* ptr = cmd;
    newCommandWin(ptr++, 12, 8, 22, "Set selected bit", COLOR_RED, onSetSelected);
    newCommandWin(ptr++, 15, 8, 22, "Clear selected bit", COLOR_RED, onClearSelected);
    newCommandWin(ptr++, 18, 8, 22, "Flip selected bit", COLOR_RED, onFlipSelected);

    newCommandWin(ptr++, 12, 30, 22, "Set other bits", COLOR_GREEN, onSetOther);
    newCommandWin(ptr++, 15, 30, 22, "Clear other bits", COLOR_GREEN, onClearOther);
    newCommandWin(ptr++, 18, 30, 22, "Flip other bits", COLOR_GREEN, onFlipOther);

    newCommandWin(ptr++, 12, 52, 21, "Set all bits", COLOR_CYAN, onSetAll);
    newCommandWin(ptr++, 15, 52, 21, "Clear all bits", COLOR_CYAN, onClearAll);
    newCommandWin(ptr++, 18, 52, 21, "Flip all bits", COLOR_CYAN, onFlipAll);

    eventLoop();
    closeInterface();
    return EXIT_SUCCESS;
}

