#include <stdlib.h>
#include "interface.h"

extern bitword input;
extern bitword mask;
extern bitword output;

bitword selection = 0;

void onInputClick(int bit) {
    input ^= 1 << bit;
}

void onMaskClick(int bit) {
    mask ^= 1 << bit;
}

void onSetMasked();
void onClearMasked();
void onFlipMasked();
void onGetMasked();
void onSetUnmasked();
void onClearUnmasked();
void onFlipUnmasked();
void onGetUnmasked();

int main() {
    if (!initInterface()) {
        return EXIT_FAILURE;
    }

    BINARYWINDOW inputWin, maskWin, outputWin;
    newBinaryWin(&inputWin, 0, 6, "Input byte", COLOR_WHITE, 16, &input, &selection, onInputClick);
    newBinaryWin(&maskWin, 6, 6, "Mask", COLOR_MAGENTA, 16, &mask, &selection, onMaskClick);
    newBinaryWin(&outputWin, 19, 6, "Output byte", COLOR_WHITE, 16, &output, &selection, NULL);

    COMMANDWINDOW cmd[8];
    COMMANDWINDOW* ptr = cmd;
    newCommandWin(ptr++, 12, 5, 16, "Set masked", COLOR_RED, onSetMasked);
    newCommandWin(ptr++, 12, 21, 18, "Clear masked", COLOR_YELLOW, onClearMasked);
    newCommandWin(ptr++, 12, 39, 17, "Flip masked", COLOR_GREEN, onFlipMasked);
    newCommandWin(ptr++, 12, 56, 16, "Get masked", COLOR_CYAN, onGetMasked);

    newCommandWin(ptr++, 15, 5, 16, "Set unmasked", COLOR_RED, onSetUnmasked);
    newCommandWin(ptr++, 15, 21, 18, "Clear unmasked", COLOR_YELLOW, onClearUnmasked);
    newCommandWin(ptr++, 15, 39, 17, "Flip unmasked", COLOR_GREEN, onFlipUnmasked);
    newCommandWin(ptr++, 15, 56, 16, "Get unmasked", COLOR_CYAN, onGetUnmasked);

    eventLoop();
    closeInterface();
    return EXIT_SUCCESS;
}

